FROM golang:1.23-alpine AS builder

RUN go install github.com/onsi/ginkgo/v2/ginkgo@v2.22.2
RUN go install github.com/boumenot/gocover-cobertura@v1.3.0
RUN go install github.com/golangci/golangci-lint/cmd/golangci-lint@v1.63.4

FROM golang:1.23-alpine

RUN apk -U --no-cache add gcc libc-dev openssh
COPY --from=builder /go/bin/* /usr/local/bin/